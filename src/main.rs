use std::env;
use std::path::Path;
use std::io::{self, Read};

enum NumberingSystem {
	DEC,
	HEX,
	OCT,
	BIN,
}

fn parse_hex(str_number: &str) -> u32 {
	str_number.get(2..).unwrap().chars().rev().enumerate().map(|(i, b)|  {
		match b.to_digit(16) {
			Some(expr) => (expr as u32) << i*4,
			None => 0,
		}
	}).sum()
}

fn parse_bin(str_number: &str) -> u32 {
	str_number.get(2..).unwrap().chars().rev().enumerate().map(|(i, b)|  {
		match b.to_digit(2) {
			Some(expr) => (expr as u32) << i,
			None => 0,
		}
	}).sum()
}

fn parse_oct(str_number: &str) -> u32 {
	str_number.get(2..).unwrap().chars().rev().enumerate().map(|(i, b)|  {
		match b.to_digit(8) {
			Some(expr) => (expr as u32) << i*3,
			None => 0,
		}
	}).sum()
}

fn parse_dec(str_number: &str) -> u32 {
	let number = str_number.parse::<u32>();
		match number {
			Err(_) => 0,
			Ok(expr) => expr,
		}
}

fn get_name(arg: &String) -> String {
	let name: String;
	match Path::new(arg).file_name() {
		Some(expr) => name = (*expr).to_str().unwrap().to_string(),
		None => name = "bin".to_string(),
	}
	name
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let name: String = get_name(&args[0]);
	match name.as_ref() {
		"bin" => convert(NumberingSystem::BIN, get_number(&args)),
		"hex" => convert(NumberingSystem::HEX, get_number(&args)),
		"oct" => convert(NumberingSystem::OCT, get_number(&args)),
		"dec" => convert(NumberingSystem::DEC, get_number(&args)),
		&_ => default(&name, &args),
	}
}

fn get_number(args: &Vec<String>) -> String {
	if args.len() < 2 {
		let mut buffer = String::new();
    	match io::stdin().read_to_string(&mut buffer) {
    		Ok(_) => buffer.trim().to_string(),
    		Err(_) => "0".to_string(),
    	}
	} else {
		args[1].clone()
	}
}

fn convert(to: NumberingSystem, str_value: String) {
	let prefix = match str_value.get(0..2) {
		Some(expr) => expr,
		None => "",
	};
	let number = match prefix {
		"0x" => parse_hex(&str_value),
		"0b" => parse_bin(&str_value),
		"0o" => parse_oct(&str_value),
		_ => parse_dec(&str_value),
	};
	match to {
		NumberingSystem::HEX => {
			println!("0x{:x}", number);
		},
		NumberingSystem::OCT => {
			println!("0o{:o}", number);
		},
		NumberingSystem::BIN => {
			println!("0b{:b}", number);
		},
		NumberingSystem::DEC => {
			println!("{:}", number);	
		},
	};
}

fn default(name: &String, args: &Vec<String>) {
	if args.len() < 3 {
		println!("Usage: {} bin|hex|oct|dec <value>", name);
		return
	}
	match args[1].as_ref() {
		"bin" => convert(NumberingSystem::BIN, args[2].clone()),
		"hex" => convert(NumberingSystem::HEX, args[2].clone()),
		"oct" => convert(NumberingSystem::OCT, args[2].clone()),
		"dec" => convert(NumberingSystem::DEC, args[2].clone()),
		&_ => {
			return;
		}
	}
}
